import asyncio
from playwright.async_api import async_playwright, Playwright
from pprint import pprint

async def run(playwright: Playwright):
    chromium = playwright.chromium # or "firefox" or "webkit".
    browser = await chromium.launch()
    page = await browser.new_page()
    await page.goto("https://www.instagram.com/p/C7FSIttva2h")
    # other actions...
    # pprint((await page.content()))
    # pprint((await page.getByText('_a9zr')))
    locator = page.get_by_text("_a9zr")
    print(locator)
    await browser.close()

async def main():
    async with async_playwright() as playwright:
        await run(playwright)
asyncio.run(main())

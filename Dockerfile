FROM debian
LABEL Name=pegatick
LABEL Version=1.0
ENV TZ="America/Sao_Paulo"

LABEL description="ainda não sei"
LABEL maintainer="Luis Felipe Americo Fernandes<luisfelipeamerico@gmail.com>"

RUN apt update  && apt install  python3-dev python3-requests  libpq-dev pip -y  && rm -rf /var/lib/apt/lists/*
RUN mkdir -p /scripts/
COPY ./  /scripts/

RUN ls -l /scripts/
RUN pip3 install  -r /scripts/requirements.txt --break-system-packages
RUN playwright install chrome firefox
RUN playwright install